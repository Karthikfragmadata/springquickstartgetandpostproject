package com.fragmadata.helloApplication;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.StringReader;

@RestController
@RequestMapping(path = "/SampleController")
public class Controller {

    @RequestMapping(path = "/getName", method = RequestMethod.GET, consumes = "application/json", produces = "application/json")
    public String getName(@RequestParam(name = "name", required = false,
            defaultValue = "Unknown") String name) {

        /**
         *
         * business logic
         *
         */

        return name;
    }


    @RequestMapping(path = "/getName", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public String postTheData(@RequestParam(name = "name", required = false,
            defaultValue = "Unknown") String name) {

        /***
         *
         * business logic
         *
         */
        System.out.println("name=" + name);
        return "SUBMITTED";
    }

}



