package com.fragmadata.helloApplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloApplication {

	public static void main(String[] args) {
		System.out.println("Entered in to mail class");
		SpringApplication.run(HelloApplication.class, args);
	}

}
